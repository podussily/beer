package com.maxtropy.roc.beer;

import com.maxtropy.roc.beer.processors.*;
import com.squareup.javapoet.JavaFile;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Generator {
    public static void main(String[] args) throws IOException {
        String source = args[0];
        String outputDir = args[1];

        MyLineProcessor lineProcessor = new MyLineProcessor();
        NameCellProcessor nameCellProcessor = new NameCellProcessor();
        TypeCellProcessor typeCellProcessor = new TypeCellProcessor();
        SubTypeCellProcessor subTypeCellProcessor = new SubTypeCellProcessor();
        TopicCellProcessor topicCellProcessor = new TopicCellProcessor();
        ArgsCellProcessor argsCellProcessor = new ArgsCellProcessor();
        DescriptionProcessor descriptionProcessor = new DescriptionProcessor();
        MessageGenerator msgGenearator = new IoTMessageGenerator("com.maxtropy.roc.message");

        File sourceFile = new File(source);
        if (!sourceFile.exists()) {
            System.out.println("Source file does not exist, leave!");
            System.exit(0);
        }
        FileReader fileReader = new FileReader(sourceFile);
        BufferedReader br = new BufferedReader(fileReader);
        try {
            int count = 1;  //first line is cell define, second is separate line, drop them all, empty line would be also read.
            String line = br.readLine();
            while (line != null) {
                line = br.readLine();
                count++;
                if (count > 2) {
                    //cell 1: name type, cell 2: message type, cell 3: message sub type, cell 4: topic, cell 5: arguments, cell 6: description, others would be dropped.
                    if (line == null || "" .equals(line)) {
                        continue;
                    }
                    String[] cells = lineProcessor.cells(line);
                    String name = nameCellProcessor.fetch(cells[0]);
                    String type = typeCellProcessor.fetch(cells[1]);
                    String subType = subTypeCellProcessor.fetch(cells[2]);
                    String topic = topicCellProcessor.fetch(cells[3]);
                    ArgsCellProcessor.Argument[] arguments = argsCellProcessor.arguments(cells[4]);
                    String description = descriptionProcessor.convert(cells[5]);
                    JavaFile javaFile = msgGenearator.generateV2(name, type, subType, topic, arguments, description);
                    //写到的是项目的根目录
                    File opD = new File(outputDir);
                    if (!opD.exists()) opD.mkdir();
                    System.out.println("Writing file to " + opD.getAbsolutePath());
                    javaFile.writeTo(opD);
                }
            }
        } finally {
            br.close();
        }
    }
}
