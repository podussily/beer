package com.maxtropy.roc.beer.processors;

import com.squareup.javapoet.JavaFile;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class MyClass {

    public static void main(String[] args) throws IOException {
        LineProcessor lineProcessor = new MyLineProcessor();
        NameCellProcessor nameCellProcessor = new NameCellProcessor();
        TypeCellProcessor typeCellProcessor = new TypeCellProcessor();
        SubTypeCellProcessor subTypeCellProcessor = new SubTypeCellProcessor();
        TopicCellProcessor topicCellProcessor = new TopicCellProcessor();
        ArgsCellProcessor argsCellProcessor = new ArgsCellProcessor();
        DescriptionProcessor descriptionProcessor = new DescriptionProcessor();
        MessageGenerator msgGenearator = new IoTMessageGenerator("com.maxtropy.roc.message");

        FileReader fileReader = new FileReader(new File("w.md"));
        BufferedReader br = new BufferedReader(fileReader);
        try {
            int count = 1;  //first line is cell define, second is separate line, drop them all, empty line would be also read.
            String line = br.readLine();
            while (line != null) {
                line = br.readLine();
                count++;
                if (count > 2) {
                    //cell 1: name type, cell 2: message type, cell 3: message sub type, cell 4: topic, cell 5: arguments, cell 6: description, others would be dropped.
                    if (line == null || "" .equals(line)) {
                        continue;
                    }
                    String[] cells = lineProcessor.cells(line);
                    for (String s : cells) {
                        System.out.println("----s:" + s);
                    }
                    System.out.println("cells.size: " + cells.length);
                    String name = nameCellProcessor.fetch(cells[0]);
                    String type = typeCellProcessor.fetch(cells[1]);
                    System.out.println("------type:" + type);
                    String subType = subTypeCellProcessor.fetch(cells[2]);
                    System.out.println("++++++subType:" + subType);
                    String topic = topicCellProcessor.fetch(cells[3]);
                    System.out.println("+++++++topic:" + topic);
                    ArgsCellProcessor.Argument[] arguments = argsCellProcessor.arguments(cells[4]);
                    System.out.println("arguments.length: " + arguments.length);
                    for (ArgsCellProcessor.Argument argument : arguments) {
                        System.out.println("Argument's.toString():" + argument.toString());
                    }
                    String description = descriptionProcessor.convert(cells[5]);
                    System.out.println("======description: \n" + description);
                    JavaFile javaFile = msgGenearator.generateV2(name, type, subType, topic, arguments, description);
                    //写到的是项目的根目录
                    javaFile.writeTo(new File("com.example.main"));
                }
            }
        } finally {
            br.close();
        }
    }
}
