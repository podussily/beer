package com.maxtropy.roc.beer.processors;


public class NameCellProcessor {
    public String fetch(String cell) {
        return cell.replaceAll("\\s", "").split("\\s")[0];
    }
}
