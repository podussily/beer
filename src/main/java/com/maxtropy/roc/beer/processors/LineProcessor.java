package com.maxtropy.roc.beer.processors;

public interface LineProcessor {
    String[] cells(String line);
}
