package com.maxtropy.roc.beer.processors;

public interface CellProcessor {
    String[] elements(String cell);
}
