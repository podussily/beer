package com.maxtropy.roc.beer.processors;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeName;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class ArgsCellProcessor {
    public Argument[] arguments(String cell) {
        String[] args = cell.split("<br>");
        if (args.length == 0) return null;
        boolean beforeComment;
        boolean numberIDFinished;
        boolean typeFinished;
        boolean attrFinished;
        Argument[] results = new Argument[args.length];

        StringBuilder numberIDBuilder;
        StringBuilder typeBuilder;
        StringBuilder attrBuilder;
        String preComment;
        int index = 0;

        for (String arg : args) {
            beforeComment = true;
            numberIDFinished = false;
            typeFinished = false;
            attrFinished = false;

            numberIDBuilder = new StringBuilder();
            typeBuilder = new StringBuilder();
            attrBuilder = new StringBuilder();

            Argument a = null;

            for (int i = 0; i < arg.length(); i++) {
                //1. 2. 数字编号之前的空格需要忽略
                if (beforeComment && 32 == arg.charAt(i) && !numberIDFinished) continue;
                if (!numberIDFinished && arg.charAt(i) > 47 && arg.charAt(i) < 58) {
                    numberIDBuilder.append(arg.charAt(i));
                }
                //. dot means the number is over.
                if (arg.charAt(i) == 46) {
                    numberIDFinished = true;
                    continue;
                }
                //使用空格（32）来间隔Type definition和Name definition
                if (!typeFinished && arg.charAt(i) != 32 && numberIDFinished) {
                    typeBuilder.append(arg.charAt(i));
                } else if (!typeFinished && numberIDFinished && typeBuilder.length() > 0) {
                    //这里是Type和属性名之间的空格
                    typeFinished = true;
                    continue;
                } else if (!typeFinished && numberIDFinished && typeBuilder.length() == 0) {
                    //这里是Type之前的空格
                    continue;
                }
                // 符号//之前，且不为空格的字符都为属性名
                if (!attrFinished && arg.charAt(i) != 32
                        && typeFinished && arg.charAt(i) != 47) {
                    attrBuilder.append(arg.charAt(i));
                } else if (!attrFinished && typeFinished) {
                    attrFinished = true;
                    beforeComment = false;
                    preComment = arg.substring(i);
                    a = new Argument(Argument.findTypes(typeBuilder.toString()), attrBuilder.toString(), preComment);
                }
            }
            results[index] = a;
            index++;
        }
        return results;
    }

    public static final class Argument {
        TypeName classNames;
        String attrName;
        String comment;

        public Argument(TypeName classNames, String attrName, String comment) {
            this.classNames = classNames;
            this.attrName = attrName;
            this.comment = comment;
        }

        private static TypeName findTypes(String plainType) {
            String lowerCaseType = plainType.toLowerCase();
            switch (lowerCaseType) {
                case "int":
                case "integer":
                case "long":
                case "float":
                case "double":
                case "boolean":
                case "char":
                case "character":
                case "short":
                case "byte":
                case "string":
                    return plainToType(lowerCaseType);
                default:
                    //Here handle non-primitive type.
                    if (lowerCaseType.startsWith("list")) {
                        Pattern pattern = Pattern.compile("[a-zA-Z]+");
                        Matcher matcher = pattern.matcher(lowerCaseType.substring(4));
                        if (matcher.find()) {
                            return ParameterizedTypeName.get(
                                    ClassName.get("java.util", "List"),
                                    plainToType(matcher.group(0)));
                        }
                    }
                    return null;
            }
        }

        private static ClassName plainToType(String plain) {
            switch (plain) {
                case "int":
                    return ClassName.get("java.lang", "Integer");
                case "integer":
                    return ClassName.get("java.lang", "Integer");
                case "boolean":
                    return ClassName.get("java.lang", "Boolean");
                case "float":
                    return ClassName.get("java.lang", "Float");
                case "double":
                    return ClassName.get("java.lang", "Double");
                case "long":
                    return ClassName.get("java.lang", "Long");
                case "short":
                    return ClassName.get("java.lang", "Short");
                case "byte":
                    return ClassName.get("java.lang", "Byte");
                case "char":
                    return ClassName.get("java.lang", "Character");
                case "character":
                    return ClassName.get("java.lang", "Character");
                case "string":
                    return ClassName.get("java.lang", "String");
                default:
                    return null;
            }
        }

        @Override
        public String toString() {
            return "Argument{" +
                    "classNames=" + classNames +
                    ", attrName='" + attrName + '\'' +
                    ", comment='" + comment + '\'' +
                    '}';
        }
    }
}
