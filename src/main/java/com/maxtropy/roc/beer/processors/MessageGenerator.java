package com.maxtropy.roc.beer.processors;

import com.squareup.javapoet.JavaFile;

public interface MessageGenerator {
    JavaFile generateV2(String name, String type, String subType, String topic, ArgsCellProcessor.Argument[] args, String description);
}
