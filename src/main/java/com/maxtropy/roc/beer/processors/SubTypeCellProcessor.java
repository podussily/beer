package com.maxtropy.roc.beer.processors;

public final class SubTypeCellProcessor {
    public String fetch(String cell) {
        return cell.replaceAll("\\s", "").split("\\s")[0];
    }
}
