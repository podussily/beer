package com.maxtropy.roc.beer.processors;

import com.maxtropy.mockingbirds.annotation.MessageKey;
import com.maxtropy.mockingbirds.annotation.MessageType;
import com.maxtropy.mockingbirds.protocol.AbstractMessageV2;
import com.maxtropy.mockingbirds.protocol.MessageConstV2;
import com.squareup.javapoet.*;

import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import java.util.*;

public class IoTMessageGenerator implements MessageGenerator {

    private String packageName;

    public IoTMessageGenerator(String packageName) {
        this.packageName = packageName;
    }

    @Override
    public JavaFile generateV2(String name, String type, String subType, String topic, ArgsCellProcessor.Argument[] args, String description) {
        //1. const
        FieldSpec subTypeConst = FieldSpec.builder(int.class, "SUB_TYPE")
                .addModifiers(Modifier.PUBLIC, Modifier.FINAL, Modifier.STATIC)
                .initializer(subType)
                .build();
        String captitalT;
        String lowType = type.toLowerCase();
        if (lowType.contains("request")) {
            captitalT = "Request";
        } else if (lowType.contains("response")) {
            captitalT = "Response";
        } else if (lowType.contains("report")) {
            captitalT = "Report";
        } else {
            throw new IllegalArgumentException("Error type define.");
        }
        FieldSpec typeConst = FieldSpec.builder(int.class, "TYPE")
                .addModifiers(Modifier.PUBLIC, Modifier.FINAL, Modifier.STATIC)
                .initializer("$T." + "TYPE_" + captitalT.toUpperCase(), MessageConstV2.class)
                .build();
        //目前只是将topic放入到常量当中，对于用于接收的消息，并不需要设置topic, 而自己主动发送的消息很可能需要设置topic,
        //因此将topic的使用逻辑下放到业务当中。
        FieldSpec topicConst = FieldSpec.builder(String.class, "TOPIC")
                .addModifiers(Modifier.PUBLIC, Modifier.FINAL, Modifier.STATIC)
                .initializer("$S", topic)
                .build();

        //2. Mockingbird message annotations
        AnnotationSpec messageType = AnnotationSpec.builder(MessageType.class)
                .addMember("subType", (name + captitalT) + ".SUB_TYPE")
                .addMember("type", "$T." + "TYPE_" + captitalT.toUpperCase(), MessageConstV2.class)
                .build();

        //3. Fields and Methods, using LinkedHashSet to ensure generated order.
        Set<FieldSpec> fieldSpecs = new LinkedHashSet<>();
        Set<MethodSpec> methodSpecs = new LinkedHashSet<>();
        //当没有argument时会解析出长度为1，内容为null的args.
        if (args != null && args.length > 1) {
            int i = 1;
            for (ArgsCellProcessor.Argument arg : args) {
                AnnotationSpec messageKey = AnnotationSpec.builder(MessageKey.class)
                        .addMember("value", "$L", i)
                        .build();
                FieldSpec fieldSpec = FieldSpec.builder(arg.classNames, arg.attrName)
                        .addModifiers(Modifier.PRIVATE)
                        .addAnnotation(messageKey)
                        .addJavadoc(arg.comment)
                        .build();
                fieldSpecs.add(fieldSpec);
                MethodSpec getter = MethodSpec.methodBuilder((arg.classNames == TypeName.BOOLEAN ? "is" : "get") + arg.attrName.toUpperCase().charAt(0) + arg.attrName.substring(1))
                        .addModifiers(Modifier.PUBLIC)
                        .addCode("return " + arg.attrName + ";\n")
                        .returns(arg.classNames)
                        .build();
                MethodSpec setter = MethodSpec.methodBuilder("set" + arg.attrName.toUpperCase().charAt(0) + arg.attrName.substring(1))
                        .addModifiers(Modifier.PUBLIC)
                        .addParameter(arg.classNames, arg.attrName)
                        .addCode("this." + arg.attrName + " = " + arg.attrName + ";\n")
                        .build();
                methodSpecs.add(getter);
                methodSpecs.add(setter);
                i++;
            }
        }

        //4. Class Type
        TypeSpec typeSpec = TypeSpec
                .classBuilder(toHumpName(name, captitalT))
                .addModifiers(Modifier.PUBLIC, Modifier.FINAL)
                .superclass(AbstractMessageV2.class)
                .addField(subTypeConst)
                .addField(typeConst)
                .addField(topicConst)
                .addFields(fieldSpecs)
                .addMethods(methodSpecs)
                .addAnnotation(messageType)
                .build();

        return JavaFile.builder(packageName, typeSpec).build();
    }

    public static String toHumpName(String firstName, String messageType) {
        //e.g fistName = MachineType, messageType = Request
        String rest = messageType.substring(1);
        return firstName + messageType.charAt(0) + rest.toLowerCase();
    }
}
