package com.maxtropy.roc.beer.processors;

public final class DescriptionProcessor{

    public String convert(String description) {
        return description.replaceAll("<br>", "\n");
    }
}
