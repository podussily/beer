package com.maxtropy.roc.beer.processors;

public class MyLineProcessor implements LineProcessor {

    /**
     * The first one is always empty.
     *
     * @param line
     * @return
     */
    @Override
    public String[] cells(String line) {
        String[] pre = line.split("\\|");
        if (pre.length == 1) return null;
        String[] rear = new String[pre.length - 1];
        System.arraycopy(pre, 1, rear, 0, pre.length - 1);
        return rear;
    }
}
