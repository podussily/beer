package com.maxtropy.roc.beer;

import com.squareup.javapoet.JavaFile;
import org.gradle.api.DefaultTask;
import org.gradle.api.tasks.TaskAction;

import java.io.File;
import java.io.IOException;

public class Beer extends DefaultTask {
    private String sourcePath;
    private String classDir;

    public String getSourcePath() {
        return sourcePath;
    }

    public void setSourcePath(String sourcePath) {
        this.sourcePath = sourcePath;
    }

    public String getClassDir() {
        return classDir;
    }

    public void setClassDir(String classDir) {
        this.classDir = classDir;
    }

    @TaskAction
    void generateMessage() {
        sourcePath = sourcePath == null ? String.valueOf(getProject().findProperty("sourcePath")) : sourcePath;
        classDir = classDir == null ? String.valueOf(getProject().findProperty("classDir")) : classDir;
        File sourceFile = new File(java.lang.String.valueOf(sourcePath));
        File outDir = new File(java.lang.String.valueOf(classDir));
        JavaFile a;
        System.out.println("Start generating. " + sourceFile.getAbsolutePath() + " " + outDir.getAbsolutePath());
        if (!sourceFile.exists()) {
            System.out.println("Source file does not exist. Leaving......");
        }
        if (!outDir.exists()) {
            System.out.println("Making output class dir......");
            outDir.mkdir();
        }
        String[] args = new String[]{sourcePath, classDir};
        try {
            Generator.main(args);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
