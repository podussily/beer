package com.maxtropy.roc.beer;

import com.maxtropy.mockingbirds.protocol.MessageV2;
import org.gradle.api.Plugin;
import org.gradle.api.Project;

public class BeerPlugin implements Plugin<Project> {
    @Override
    public void apply(Project project) {
        System.out.println("1.0 key args:  " + project.findProperty("sourcePath") + " " + project.findProperty("classDir"));
        project.getTasks().create("beer", Beer.class);
    }
}
